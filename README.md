# Baby Project UI

Ceci est un projet personnel réalisé pour l'apprentisage de Angular 8, nodeJS, TS...

Le but est d'avoir un client web de l'API BB-projet-API clair, responsive et efficace.



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## installation 

  - installer  [nodeJS](https://nodejs.org/fr/) sur son poste
  - cloner le projet
  - Dans le projet via un terminal : `npm install`
  - Lancement du projet : `npm start`

## V0.3.0 Vote du prénom et paramétrage

Compatible avec la version 0.3.0 de l'API.

## V0.2.2 [UI] Dashboard suite

Amélioration de l'affichage en mode grand écran.  
Amélioration de l'accordion cliquable sur toute la surface, et plus seulement le nom.  
Ajout d'un lien pour revenir en haut de page tous les 10 votes.

## V0.2.1 [UI] DashBoard

Amélioration de l'UI pour la zone parents avec un accordion.

Correction du titre de la zone parents. 

## V0.2.0 Auth & DashBoard

Gestion de l'API sécurisée avec Basic Auth.  
Création d'une page de login et d'un lien de deconnexion.

Création de la zone parent qui recense les votes gender.  
Gestion des roles de l'utilisateur pour le prévenir de ses limitations et de l'empecher de supprimer un vote.


## V0.1.0 Gender

Création de la base du client avec : 

  * Une page de welcome mettant en avant le choix du vote du genre du bébé
  * Une page de remplissage de formulaire de vote
  * Une page pour les CGU avec un début de gestion de la RGPD

## A venir

  * Gestion des paramètres fonctionnels (Définir le sexe du bébé, publier l'information, Définir les prénoms du bébé, publier l'information..)
  * Page de vote pour les prénoms
  * tests
