import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { Router, ActivatedRoute, UrlSegment } from '@angular/router';
import { AuthService } from '@bbproject/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public isParentZone: boolean = false;
  public showTitle: boolean = true;
  public userRole: string;
  public currentUser: string;

  constructor(private location: Location, private router: Router, private userService: AuthService) {
    router.events.subscribe(val => {
      if (location.path() != "" && location.path().startsWith("/parents")) {
        this.isParentZone = true;
      } else {
        this.isParentZone = false;
      }
      if(location.path().startsWith("/auth") || location.path().startsWith("/not-found") ){
        this.showTitle = false;
      } else {
        this.showTitle = true;
      }
      
    });

    this.userRole = this.userService.currentUserValue != null ? this.userService.currentUserValue.role: "";
    this.currentUser = this.userService.currentUserValue != null ? this.userService.currentUserValue.username: "";
  }

  ngOnInit() {

  }

  onLogOut(){
    this.userService.logout();
    window.location.replace("/");
  }
}
