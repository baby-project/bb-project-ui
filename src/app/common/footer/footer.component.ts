import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '@bbproject/models';
import { AuthService } from '@bbproject/services';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  version: string = `${environment.version}`;
  currentUser: User;
  
  constructor(private router: Router,
    private authService: AuthService) { 
      this.authService.currentUser.subscribe(x => this.currentUser = x);
    }

  ngOnInit() {
  }

  onLogOut(){
    this.authService.logout();
    window.location.replace("/");
  }
}
