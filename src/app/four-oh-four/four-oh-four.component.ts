import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-four-oh-four',
  templateUrl: './four-oh-four.component.html',
  styleUrls: ['./four-oh-four.component.css']
})
export class FourOhFourComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.setTitle("404 NOT FOUND")
  }

  setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

}
