import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Setting } from '@bbproject/models/setting';
import { NgxSpinnerService } from 'ngx-spinner';
import { SettingService } from '@bbproject/services/setting.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  genderSetting: Setting;
  nameSetting: Setting = new Setting("name", "", false, this.genderSetting);

  spinnerTxt: string = "Chargement...";
  genderTxt: string;
  accord: string = "";

  constructor(private route: Router,
    private settingService: SettingService,
    private spinner: NgxSpinnerService,
    private titleService: Title) {

  }

  ngOnInit() {
    this.spinner.show();
    this.settingService.loadAllSettings().subscribe(
      (data) => {
        data.forEach(setting => {
          if (setting.key == "gender") { this.genderSetting = setting; }
          if (setting.key == "name") { this.nameSetting = setting; }
        });
        this.defineScreenAndData();
        this.spinner.hide();
      },
      (error) => {
        console.error(error);
        this.spinner.hide();
      }
    );



    this.setTitle("Welcome");
  }

  defineScreenAndData() {

    const divGender = document.getElementById("gender");
    const divName = document.getElementById("name");
    const divEnd = document.getElementById("end");
    const btnNameBet = document.getElementById("nameBet");

    if (this.nameSetting.revealed) {
      divEnd.classList.replace("hidden", "show");
      divName.classList.replace("show", "hidden");
      divGender.classList.replace("show", "hidden");
      if (this.genderSetting.value == 'G') {this.accord = "e";}
    } else if (this.genderSetting.revealed) {
      divName.classList.replace("hidden", "show");
      divEnd.classList.replace("show", "hidden");
      divGender.classList.replace("show", "hidden");

      if (this.genderSetting.value == 'G') {
        this.genderTxt = "futur fille";
        btnNameBet.classList.replace("btn-boy", "btn-girl");
      } else {
        this.genderTxt = "futur garçon";
      }

    } else {
      divGender.classList.replace("hidden", "show");
      divName.classList.replace("show", "hidden");
      divEnd.classList.replace("show", "hidden");
    }
  }

  setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  onChoice(choice: string) {

    if ('B' === choice) {
      this.route.navigate(["/gender", "B"]);
    } else if ('G' === choice) {
      this.route.navigate(["/gender", "G"]);
    }
  }

  onBet() {
    console.log("route not activated");
    this.route.navigate(["/name"]);
  }

}
