import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { FormName } from '@bbproject/models/Form-Name.model';
import { environment } from '@environments/environment';
import { Stats } from '@bbproject/models/Stats.model';

@Injectable({
  providedIn: 'root'
})
export class NameService {

  nameSubjects = new Subject<any[]>();

  private names = [];

  constructor(private httpClient: HttpClient) { }

  loadAllFormsName() {
    this.httpClient
      .get<any[]>(`${environment.apiUrl}/names`)
      .subscribe(
        (response) => {
          this.names = response;
          this.emitNameSubject();
        },
        (error) => { console.error('GET Erreur !! : ' + error);}
       );
  }

  getStats(){
    return this.httpClient
    .get<Stats>(`${environment.apiUrl}/names/stats`);
  }

  emitNameSubject() {
    if (null !== this.names){this.nameSubjects.next(this.names.slice());}
    else {this.names = [];}
    
  }

  saveForm(formName: FormName): Observable<FormName> {
    return this.httpClient.post<FormName>('rest/names/', formName);
  }

  patchName(id: number){
    return this.httpClient
    .patch(`${environment.apiUrl}/names/${id}`, null);
    
  }

  deleteName(id: number){
    return this.httpClient.delete(`${environment.apiUrl}/names/${id}`);
  }

  getNumberOfWinner(): number {
    let retour: number = 0;
    this.names.forEach(formName => {
      if(formName.findedName){retour++;}
    });
    return retour;
  }
}
