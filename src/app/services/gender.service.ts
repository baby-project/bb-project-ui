import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGender } from '@bbproject/models/Form-Gender.model';
import { environment } from '@environments/environment';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class GenderService {

  genderSubjects = new Subject<any[]>();

  private genders = [];

  constructor(private httpClient: HttpClient) { }

  loadAllFormsGender(){
    this.httpClient
      .get<any[]>(`${environment.apiUrl}/gender`)
      .subscribe(
        (response) => {
          this.genders = response;
          this.emitGenderSubject();
        },
        (error) => {
          console.error('GET Erreur !! : ' + error);
        }
      );
  }

  patchGender(id: number){
    return this.httpClient
    .patch(`${environment.apiUrl}/gender/${id}`, null);
    
  }

  deleteGender(id: number){
    return this.httpClient.delete(`${environment.apiUrl}/gender/${id}`);
  }

  emitGenderSubject() {
    if(null !== this.genders){this.genderSubjects.next(this.genders.slice());}
    else {this.genders = [];}
    
  }

  saveForm(formGender: FormGender): Observable<FormGender> {
    return this.httpClient.post<FormGender>('rest/gender/', formGender);
  }

  getNumberOf(choice: String): number {
    var retour: number = 0;
    this.genders.forEach(gender => {
      if(gender.status && choice === gender.choice) retour++;
      
    });

    return retour;
  }

}
