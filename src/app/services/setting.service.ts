import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Setting } from '@bbproject/models/setting';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SettingService {

    private settingSubjects : BehaviorSubject<Setting[]>;

    public settings :  Observable<Setting[]>;

    constructor(private httpClient: HttpClient){ 
        this.settingSubjects = new BehaviorSubject<Setting[]>(JSON.parse(localStorage.getItem('settings')));
        this.settings = this.settingSubjects.asObservable();
     }

     public get currentUserValue(): Setting[] {
        return this.settingSubjects.value;
      }

    loadAllSettings() {
        return this.httpClient
        .get<any[]>(`${environment.apiUrl}/settings`)
        .pipe(map(setting => {
                localStorage.setItem('settings', JSON.stringify(setting));
                this.settingSubjects.next(setting);
                this.settings = this.settingSubjects.asObservable();
                return setting;
            }));
    }

    patchSetting(setting: Setting){
        return this.httpClient
        .patch(`${environment.apiUrl}/settings`, setting);
        
      }
}