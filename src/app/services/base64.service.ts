import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Base64Service {

  constructor() { }

  /**
   * 
   * @param message la chaine base64 à decoder
   * @returns un tableau de string ou null
   */
  public decode(message: string) : string[]{

    let retour = null;

    const decode = atob(message).split(";");
    if(4 == decode.length){
    const length = decode[1].length + decode[2].length + decode[3].length + 3;
      if(length == parseInt(decode[0], 10)){
        retour = [decode[1],decode[2],decode[3]];
      }
    }
    
    return retour;
  }
}
