import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatGridListModule } from '@angular/material/grid-list'
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { AuthGuard, BasicAuthInterceptor, ErrorInterceptor } from '@bbproject/_helpers';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AdmingenderComponent } from './admin/admingender/admingender.component';
import { AuthComponent } from './admin/auth/auth.component';
import { AppComponent } from './app.component';
import { FooterComponent } from './common/footer/footer.component';
import { HeaderComponent } from './common/header/header.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthService } from './services/auth.service';
import { GenderService } from './services/gender.service';
import { CguComponent } from './user/cgu/cgu.component';
import { FormGenderComponent } from './user/form-gender/form-gender.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ConfirmationDialogComponent } from './common/confirmation-dialog/confirmation-dialog.component';
import { WelcomeAdminComponent } from './admin/welcome-admin/welcome-admin.component';
import { SettingsComponent } from './admin/settings/settings.component';
import { SettingService } from './services';
import { FormNameComponent } from './user/form-name/form-name.component';
import { AdminnameComponent } from './admin/adminname/adminname.component';


const appRoutes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: '', component: WelcomeComponent },
  { path: 'cgu', component: CguComponent},
  { path: 'gender', component: FormGenderComponent},
  { path: 'gender/:choice', component: FormGenderComponent},
  { path: 'name', component: FormNameComponent},
  { path: 'auth', component: AuthComponent},
  { path: 'parents', canActivate: [AuthGuard], component: WelcomeAdminComponent},
  { path: 'parents/gender', canActivate: [AuthGuard],component: AdmingenderComponent},
  { path: 'parents/name', canActivate: [AuthGuard],component: AdminnameComponent},
  { path: 'parents/settings', canActivate: [AuthGuard], component: SettingsComponent},
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' }
];

const routerOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
  // ...any other options you'd like to use
};

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    FormGenderComponent,
    AuthComponent,
    FourOhFourComponent,
    HeaderComponent,
    FooterComponent,
    CguComponent,
    AdmingenderComponent,
    ConfirmationDialogComponent,
    WelcomeAdminComponent,
    SettingsComponent,
    FormNameComponent,
    AdminnameComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes,routerOptions),
    BrowserAnimationsModule,
    NgxSpinnerModule, /* for using spinner */
    MatCardModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatButtonModule,
    MatGridListModule,
  
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [
    AuthService,
    AuthGuard,
    GenderService,
    SettingService,
    Title,
    {provide: LOCALE_ID, useValue: 'fr' },
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

  ],
  bootstrap: [AppComponent]
})
export class AppModule {  }
