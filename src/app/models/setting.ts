export class Setting {

    constructor(public key: string, public value: string, public revealed: boolean, public parent?: Setting) {
        if (null === parent) {
            this.parent = null;
        }
    }


}