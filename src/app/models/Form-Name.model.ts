import { FormBase } from './Form-Base.model';

export class FormName extends FormBase {

    constructor(
        public name:string, 
        public fname: string, 
        public email: string, 
        public choix: string[],  
        public registerDate: Date, 
        public findedName?: boolean,
        public status?: boolean, 
        public id?: number,


    ){
        super(name, fname, email, registerDate, status, id);
    }
}