export class FormBase {

    constructor(public name:string, public fname: string, public email: string, public registerDate?: Date, public status?: boolean, public id?: number){
        if( null === id){
            this.id = 0;
        }
        if( null === registerDate) {
            this.registerDate = new Date();
        }
        if( null === status){
            this.status = true; 
        }
    }


}