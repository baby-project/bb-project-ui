import { FormBase } from './Form-Base.model';

export class FormGender extends FormBase {

    constructor(
        public name:string, 
        public fname: string, 
        public email: string, 
        public choice: string,  
        public registerDate: Date, 
        public status?: boolean, 
        public id?: number){
        super(name, fname, email, registerDate, status,id);
    }
}