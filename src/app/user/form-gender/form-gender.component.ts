import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';

import { FormGender } from '@bbproject/models/Form-Gender.model';
import { GenderService } from '@bbproject/services/gender.service';

@Component({
  selector: 'app-form-gender',
  templateUrl: './form-gender.component.html',
  styleUrls: ['./form-gender.component.css']
})
export class FormGenderComponent implements OnInit {
  choice: string;
  choiceTxt: string;
  choiceColor: string;
  
  genderForm: FormGroup;
  
  divChoiceBElt : any;
  divChoiceGElt : any;
  divCompleteForm : any;
  divSuccess : any;
  divError : any;
  
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private genderService: GenderService,
    private titleService: Title ) { }
    
    ngOnInit() {
      this.initForm();
      
      /* initialisation des éléments du DOM */
      this.divChoiceBElt = document.getElementById("choiceB"); 
      this.divChoiceGElt = document.getElementById("choiceG"); 
      this.divCompleteForm = document.getElementById("completeForm"); 
      this.divSuccess = document.getElementById("success");
      this.divError = document.getElementById("errorBlock");
      
      /* on récupère le choix de l'utilisateur */
      this.onChangeChoice(this.route.snapshot.params['choice'] !== '' ? this.route.snapshot.params['choice']: 'G');
      
      /* Gestion du titre de la page */
      this.setTitle("Fille ou Garçon ?");
    }

    public setTitle( newTitle: string) {
      this.titleService.setTitle( newTitle );
    }
    
    /**
    * Création du formulaire avec ses contraintes
    */
    initForm(){
      this.genderForm = this.formBuilder.group({
        name: ['', Validators.required],
        fname: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]]
      })
    }
    
    /**
    * Changements visuels et sauvegarde du choix de l'utilisateur
    * @param choice B ou par defaut G
    */
    onChangeChoice(choice: string){
      console.info(choice);
      if("B" === choice){
        this.choice = "B";
        this.choiceTxt = "un garçon";
        this.choiceColor = "font-boy";
        
        // on rend le choix "garçon" non selectionnable
        this.divChoiceBElt.classList.add("disabled");
        this.divChoiceBElt.setAttribute("disabled","disabled");
        
        // on rend le choix "fille" selectionnable
        this.divChoiceGElt.classList.remove("disabled");
        this.divChoiceGElt.removeAttribute("disabled");
        
      } else {
        this.choice = "G";
        this.choiceTxt = "une fille";
        this.choiceColor = "font-girl";
        
        // on rend le choix "fille" non selectionnable
        this.divChoiceGElt.classList.add("disabled");
        this.divChoiceGElt.setAttribute("disabled","disabled");
        
        // on rend le choix "garçon" selectionnable
        this.divChoiceBElt.classList.remove("disabled");
        this.divChoiceBElt.removeAttribute("disabled");
        
      }
    }
    
    onSubmitForm(){
      this.spinner.show();
      this.hideError();

      const formValue = this.genderForm.value;

      if(this.genderForm.valid){
        // on envoie le formulaire
        const formGender = new FormGender(formValue['name'],formValue['fname'],formValue['email'],this.choice, new Date());
        formGender.status = true; // on force le statut à true par défaut lors du vote
        this.genderService.saveForm(formGender).subscribe(
          (result: FormGender) => {
            this.hideForm();
            this.showSuccess(formGender);
            this.spinner.hide();
          },
          error => {
            console.error(error.status);
            console.debug("?" + error.message || error.statusText)

            switch(error.status){
              case 400: this.showError("Le formulaire est mal rempli"); break;
              case 403: this.showError("Le vote du sexe du bébé n'est plus permis.");break; 
              case 409: this.showError("Tu as déjà voté avec cette adresse email."); break;
              case 500:
              case 501:
              case 502:
              case 503:
              case 504: this.showError("Le serveur ne répond pas, ré-essaye plus tard. (code erreur " + error.status +")"); break;
              default: this.showError("Une erreur inconnue est survenue, ré-essaye plus tard");
            }
            // affichage d'une erreure
            
            this.spinner.hide();
            
          }
          );
        } else {
          // il y a un soucis avec les données saisies
          this.showError("Tu n'as pas rempli tous les champs du formulaire ou ton adresse email est incorrecte.")
          this.spinner.hide();
        }
      }
      
      /**
      * Masquage du formulaire une fois qu'il a été soumis et validé
      */
      hideForm(){
        this.divCompleteForm.style.display = "none"
      }
      
      /**
      * Affichage du message de succes pour l'utilisateur
      * 
      * @param formGender le formulaire de l'utilisateur
      */
      showSuccess(formGender: FormGender) {
        let divElt = document.createElement("div");
        divElt.classList.add("success-block");
        const ligne1: string = "Félicitations " + formGender.fname + " !!";
        const ligne2: string = "Tu as voté pour " + this.choiceTxt + ".";
        const ligne3: string = "Nous te dirons prochainement si tu avais raison :)";
        divElt.innerHTML = "<p class=\"txt-center\">" + ligne1 + "<br />" + ligne2 + "<br />" + ligne3 + "</p>" ;
        this.divSuccess.appendChild(divElt);
      }

      showError(value: string){
        this.divError.classList.replace("hidden","show");
        this.divError.innerHTML = "<p class=\"txt-center\">" + value + "</p>";
      }

      hideError(){
        this.divError.classList.replace("show","hidden");
      }
    }
    
    