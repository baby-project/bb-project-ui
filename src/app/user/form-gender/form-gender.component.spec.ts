import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGenderComponent } from './form-gender.component';

describe('FormGenderComponent', () => {
  let component: FormGenderComponent;
  let fixture: ComponentFixture<FormGenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormGenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
