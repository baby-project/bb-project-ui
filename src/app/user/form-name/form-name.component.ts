import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Base64Service } from '@bbproject/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormName } from '@bbproject/models/Form-Name.model';
import { NameService } from '@bbproject/services/name.service';

@Component({
  selector: 'app-form-name',
  templateUrl: './form-name.component.html',
  styleUrls: ['./form-name.component.css']
})
export class FormNameComponent implements OnInit {

  nameForm: FormGroup;
  params: Params;

  divCompleteForm: any;
  divSuccess: any;
  divError: any;

  urlName: string = "";
  urlFName: string = "";
  urlEmail: string = "";

  paramUser: string;

  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private base64Service: Base64Service,
    private nameService: NameService,
    private titleService: Title) { }

  ngOnInit() {

    // récupère les paramètres d'url si ils existent
    this.route.queryParams.subscribe((params: Params) => {
      if (null !== params) {
        this.decryptParam(params);
      }
    });
    this.initForm();

    this.divCompleteForm = document.getElementById("completeNameForm");
    this.divSuccess = document.getElementById("success");
    this.divError = document.getElementById("errorBlock");

    /* Gestion du titre de la page */
    this.setTitle("Quel prénom ?");
  }

  /**
   * Création du formulaire avec ses contraintes
   */
  initForm() {
    this.nameForm = this.formBuilder.group({
      choice1: ['', Validators.required],
      choice2: [''],
      choice3: [''],
      name: [this.urlName, Validators.required],
      fname: [this.urlFName, Validators.required],
      email: [this.urlEmail, [Validators.required, Validators.email]]
    })
  }

  decryptParam(params: Params) {
    const urlUser = params['user'];
    const user = this.base64Service.decode(urlUser);
    if (null !== user) {
      this.urlName = user[0];
      this.urlFName = user[1];
      this.urlEmail = user[2];
    }
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  onSubmitForm() {
    this.spinner.show();
    this.hideError();

    const formValue = this.nameForm.value;

    if (this.nameForm.valid) {
      const choices = [formValue['choice1']]; //, formValue['choice2'], formValue['choice3']];

      if(!this.isEmpty(formValue['choice2'])){
        choices.push(formValue['choice2']);
      }

      if(!this.isEmpty(formValue['choice3'])){
        choices.push(formValue['choice3']);
      }
      
      const formName = new FormName(formValue['name'], formValue['fname'], formValue['email'], choices, new Date(),false,true);

      this.nameService.saveForm(formName).subscribe(
        (result: FormName) => {
          this.hideForm();
          this.showSuccess(formName);
          this.spinner.hide();
        },

        error => {
          console.error(error.status);
          console.debug("?" + error.message || error.statusText);

          switch (error.status) {
            case 400: this.showError("Le formulaire est mal rempli"); break;
            case 403: this.showError("Le vote du prénom du bébé n'est plus permis."); break;
            case 409: this.showError("Tu as déjà voté avec cette adresse email."); break;
            case 500:
            case 501:
            case 502:
            case 503:
            case 504: this.showError("Le serveur ne répond pas, ré-essaye plus tard. (code erreur " + error.status + ")"); break;
            default: this.showError("Une erreur inconnue est survenue, ré-essaye plus tard");
          }
          this.spinner.hide();
        }
      )

    } else {
      this.showError("Tu n'as pas rempli tous les champs du formulaire ou ton adresse email est incorrecte.")
      this.spinner.hide();
    }
  }

  /**
  * Masquage du formulaire une fois qu'il a été soumis et validé
  */
  hideForm() {
    this.divCompleteForm.style.display = "none";
  }

  /**
      * Affichage du message de succes pour l'utilisateur
      * 
      * @param formGender le formulaire de l'utilisateur
      */
  showSuccess(formName: FormName) {
    let divElt = document.createElement("div");
    divElt.classList.add("success-block");
    const ligne1: string = "Félicitations " + formName.fname + " !!";
    const ligne2: string = "Tu as voté pour : " + this.printChoices(formName.choix) + ".";
    const ligne3: string = "Nous te dirons prochainement si tu avais raison :)";
    divElt.innerHTML = "<p class=\"txt-center\">" + ligne1 + "<br />" + ligne2 + "<br />" + ligne3 + "</p>";
    this.divSuccess.appendChild(divElt);
  }

  printChoices(choices: string[]): string {
    let retour = choices[0];
    if (!this.isEmpty(choices[1]) && !this.isEmpty(choices[2])) {
      retour = retour + ", " + choices[1] + " et " + choices[2];
    } else if (!this.isEmpty(choices[1]) && this.isEmpty(choices[2])) {
      retour = retour + " et " + choices[1];
    }

    return retour;
  }

  showError(value: string) {
    this.divError.classList.replace("hidden", "show");
    this.divError.innerHTML = "<p class=\"txt-center\">" + value + "</p>";
  }

  hideError() {
    this.divError.classList.replace("show", "hidden");
  }

  isEmpty(str: string):boolean{
    return (!str || str.length === 0 || !str.trim());
  }
}
