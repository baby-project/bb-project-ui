import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-cgu',
  templateUrl: './cgu.component.html',
  styleUrls: ['./cgu.component.css']
})
export class CguComponent implements OnInit {

  constructor( private titleService: Title) { }

  ngOnInit() {
    this.setTitle("CGU");
  }

  setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
