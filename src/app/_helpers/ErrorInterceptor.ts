import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService, SettingService } from '@bbproject/services';

@Injectable({ providedIn: 'root' })
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private settingsService: SettingService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authService.logout();
                //location.reload(false);
            }
            console.log("INTERCEPTOR");
            console.log(err.body);
            console.log("?" + err.error.message || err.statusText)
            const error = err.error.message || err.statusText;
            return throwError(error);})

        return next.handle(request)
    }
}