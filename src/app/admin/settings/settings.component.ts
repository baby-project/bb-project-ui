import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { GenderService, SettingService, AuthService } from '@bbproject/services';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Setting } from '@bbproject/models/setting';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  genderForm: FormGroup;
  nameForm: FormGroup;

  genderSetting: Setting;
  nameSetting: Setting;

  divSuccess: any;
  divError: any;
  switchGender: any;

  userRole: string;

  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private settingService: SettingService,
    private userService: AuthService,
    private titleService: Title) { }

  ngOnInit() {

    this.userRole = this.userService.currentUserValue.role;

    this.initForm();

    /* initialisation des éléments du DOM */
    this.divSuccess = document.getElementById("successBlock");
    this.divError = document.getElementById("errorBlock");
    this.switchGender = document.getElementById("switchGender");

    this.settingService.loadAllSettings().subscribe(
      (data) => {
        data.forEach(setting =>{
          if (setting.key == "gender") { this.genderSetting = setting; }
          if (setting.key == "name") { this.nameSetting = setting; }
        });
        console.log(this.genderSetting);
        console.log(this.nameSetting);
      },
      (error) => {
        console.error(error);
      }
    );



    /* Gestion du titre de la page */
    this.setTitle("Parents Zone - Paramètres");
  }

  /**
    * Création des formulaire avec ses contraintes
    */
  initForm() {
    // gender form
    this.genderForm =  this.formBuilder.group({
      
      genderCheckboxes: ['', [Validators.required]],
      genderRevealed: []
    });

    // name form
    this.nameForm = this.formBuilder.group({
      nameSelect: [' ', [Validators.required]],
      nameRevealed: []
    })
  }

  onSubmitGenderForm() {
    this.spinner.show();
    this.hideError();
    this.hideSuccess();

    const genderFormValue = this.genderForm.value;

    if (this.genderForm.valid) {
      const genderSetting = new Setting("gender",genderFormValue['genderCheckboxes'],  genderFormValue['genderRevealed'], null);
      console.log("Setting : "+ genderSetting.key + " " + genderSetting.value + " " + genderSetting.revealed + " " + genderSetting.parent);
      this.settingService.patchSetting(genderSetting).subscribe(
        (response) => {
          this.settingService.loadAllSettings();
          this.showSuccess("sexe du bébé");
          this.spinner.hide();
        },
        error => {
          switch(error.status){
            case 400: this.showError("Le formulaire est mal rempli"); break;
            case 403: this.showError("Tu n'as pas les droits nécessaires pour cette action.");break; 
            case 409: this.showError("Ce paramètre est déjà révélé, il ne peux plus être modifié."); break;
            case 500:
            case 501:
            case 502:
            case 503:
            case 504: this.showError("Le serveur ne répond pas, ré-essaye plus tard. (code erreur " + error.status +")"); break;
            default: this.showError("Une erreur inconnue est survenue, ré-essaye plus tard");
          }
          this.spinner.hide();
        }
      );
    } else {
      // il y a un soucis avec les données saisies
      this.showError("Il y a un soucis avec le paramètre 'Sexe du bébé'.")
      this.spinner.hide();
    }
  }

  onSubmitNameForm() {
    this.spinner.show();
    this.hideError();
    this.hideSuccess();

    const nameFormValue = this.nameForm.value;

    if (this.nameForm.valid) {
      const nameSetting = new Setting("name",nameFormValue['nameSelect'],  nameFormValue['nameRevealed'], null);
      console.log("Setting : "+ nameSetting.key + " " + nameSetting.value + " " + nameSetting.revealed + " " + nameSetting.parent);
      this.settingService.patchSetting(nameSetting).subscribe(
        (response) => {
          this.settingService.loadAllSettings();
          this.showSuccess("prénom du bébé");
          this.spinner.hide();
        },
        error => {
          switch(error.status){
            case 400: this.showError("Le formulaire est mal rempli  ou l'étape précédente n'est pas encore révélée."); break;
            case 403: this.showError("Tu n'as pas les droits nécessaires pour cette action.");break; 
            case 409: this.showError("Ce paramètre est déjà révélé, il ne peux plus être modifié."); break;
            case 500:
            case 501:
            case 502:
            case 503:
            case 504: this.showError("Le serveur ne répond pas, ré-essaye plus tard. (code erreur " + error.status +")"); break;
            default: this.showError("Une erreur inconnue est survenue, ré-essaye plus tard");
          }
          this.spinner.hide();
        }
      );
    } else {
      // il y a un soucis avec les données saisies
      this.showError("Il y a un soucis avec le paramètre 'Prénom du bébé'.")
      this.spinner.hide();
    }
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  private onToggleRevealedGender() {
    this.genderSetting.revealed = !this.genderSetting.revealed;

  }

  private onToggleRevealedName() {
    this.nameSetting.revealed = !this.nameSetting.revealed;
  }

  /**
      * Affichage du message de succes pour l'utilisateur
      */
  showSuccess(formName: string) {
    this.divSuccess.classList.replace("hidden", "show");
    this.divSuccess.innerHTML = "<p class=\"txt-center\">Le paramètre '" + formName + "' a été mis à jour avec succès</p>";
  }

  showError(value: string) {
    this.divError.classList.replace("hidden", "show");
    this.divError.innerHTML = "<p class=\"txt-center\">" + value + "</p>";
  }

  hideError() {
    this.divError.classList.replace("show", "hidden");
  }
  hideSuccess() {
    this.divSuccess.classList.replace("show", "hidden");
  }
}
