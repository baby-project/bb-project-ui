import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-welcome-admin',
  templateUrl: './welcome-admin.component.html',
  styleUrls: ['./welcome-admin.component.css']
})
export class WelcomeAdminComponent implements OnInit {

  constructor( private titleService: Title) { }

  ngOnInit() {
    /* Gestion du titre de la page */
    this.setTitle("Parents Zone");
  }
  
  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
