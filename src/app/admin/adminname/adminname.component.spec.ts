import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminnameComponent } from './adminname.component';

describe('AdminnameComponent', () => {
  let component: AdminnameComponent;
  let fixture: ComponentFixture<AdminnameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminnameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
