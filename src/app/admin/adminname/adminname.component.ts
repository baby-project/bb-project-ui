import { Component, OnInit } from '@angular/core';
import { NameService } from '@bbproject/services/name.service';
import { AuthService } from '@bbproject/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '@bbproject/common/confirmation-dialog/confirmation-dialog.component';
import { Tendance } from '@bbproject/models/Tendance.model';
import { Stats } from '@bbproject/models/Stats.model';



@Component({
  selector: 'app-adminname',
  templateUrl: './adminname.component.html',
  styleUrls: ['./adminname.component.css']
})
export class AdminnameComponent implements OnInit {

  spinnerTxt: string = "Chargement...";

  divError: any;
  divPage: any;

  error: string = "";

  votants: number = 0;
  gagnants: number = 0;
  prenoms: number = 0;
  tendances: Tendance[];


  names: any[] = [];
  nameSubscription: Subscription;

  userRole: string;

    /* variables pour le slider toggle */
    color = 'accent';
    checked = false;
    disabled = false;
    hideStatusFalse= this.checked;


  constructor(private nameService: NameService,
    private userService: AuthService,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog,
    private titleService: Title
  ) {

  }

  ngOnInit() {

    this.spinner.show();

    this.userRole = this.userService.currentUserValue.role;

    this.divError = document.getElementById("error");
    this.divPage = document.getElementById("adminNamePage");


    this.hideError();

    this.onFetch();

    /* Gestion du titre de la page */
    this.setTitle("Parents Zone - Votes du prénom");
  }

  onFetch() {
    this.spinner.show();
    this.nameService.getStats().subscribe(
      (result: Stats) => {

        this.votants = result.numberOfVotes;
        this.gagnants = result.numberOfWinner;
        this.prenoms = result.numberOfFname;
        this.tendances = result.tendances;
        
        this.nameSubscription = this.nameService.nameSubjects.subscribe(
          (names: any[]) => {
            this.names = names;
            this.spinner.hide();
          },
          (error) => {
            this.error = error;
            console.error(error);
            this.spinner.hide();
          }
        );
        this.nameService.loadAllFormsName();
        this.nameService.emitNameSubject();

        this.divPage.classList.replace("hidden", "show");
      },
      (error) => {
        console.error(error);
        const message = error.status + " - " + error.statusText;
        this.showError(message);
        this.divPage.classList.replace("show", "hidden");
        this.spinner.hide();
      }
    );

  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  onPatch(id: number) {
    this.error = "";
    this.spinnerTxt = 'Modification en cours...';
    this.spinner.show();

    this.names = null;
    this.nameService.patchName(id).subscribe(
      (response) => {
        this.onFetch();
      },
      (error) => {
        this.error = "Une erreur est survenue, veuillez re-essayer plus tard.";
        this.spinner.hide();
      }
    );

  }

  openDialog(genderId: number, genderEmail: string): void {
    this.error = "";
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Voulez-vous vraiment supprimer le vote de '" + genderEmail + "' ?",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onDelete(genderId);
      }
    });
  }


  onDelete(id: number) {
    this.spinner.show();
    this.nameService.deleteName(id)
      .pipe(first())
      .subscribe(
        (data) => {
          this.onFetch();
          this.spinner.hide();
        },
        (error) => {
          if (error.status === 403) {
            this.error = "Vous n'avez pas les droits pour supprimer ce vote."
          } else {
            this.error = "Une erreur est survenue, veuillez re-essayer plus tard."
          }
          this.spinner.hide();
        }
      );

  }

  showError(value: string) {
    this.divError.classList.replace("hidden", "show");
    this.divError.innerHTML = "<p class=\"txt-center\">" + value + "</p>";
  }

  hideError() {
    this.divError.classList.replace("show", "hidden");
  }

  onToggleViewFalseVote() {
    this.hideStatusFalse = !this.hideStatusFalse;
    console.log("onToggleViewFalseVote => " + this.hideStatusFalse);
  }

  scrollToElement(target): void {
    console.debug(target);
    document.getElementById(target).scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  }
}
