import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GenderService } from '../../services/gender.service';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '@bbproject/services';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmationDialogComponent } from '@bbproject/common/confirmation-dialog/confirmation-dialog.component';
import { first } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-admingender',
  templateUrl: './admingender.component.html',
  styleUrls: ['./admingender.component.css']
})
export class AdmingenderComponent implements OnInit {

  
  /* variables pour le slider toggle */
  color = 'accent';
  checked = false;
  disabled = false;
  hideStatusFalse= this.checked;

  genders: any[];
  girls: number = 0;
  boys: number = 0;
  gendersSubscription: Subscription;
  userRole: string;

  /* Messages vers la page */
  error = '';
  spinnerTxt = '';

  constructor(private genderService: GenderService, 
              private userService: AuthService,
              private spinner: NgxSpinnerService,
              public dialog: MatDialog,
              private titleService: Title) { }

  ngOnInit() {
    this.spinnerTxt = 'Chargement en cours...';
    this.spinner.show();

    this.gendersSubscription = this.genderService.genderSubjects.subscribe(
      (genders: any[]) => {
        this.genders = genders;
        this.onFetch();
      }
      );
      this.genderService.emitGenderSubject();

      
      this.genderService.loadAllFormsGender();

      this.userRole = this.userService.currentUserValue.role;

      /* Gestion du titre de la page */
      this.setTitle("Parents Zone - Votes du genre");

      while(this.genders === null){
        this.genderService.emitGenderSubject();
      }

      this.spinner.hide();

    }

    public setTitle( newTitle: string) {
      this.titleService.setTitle( newTitle );
    }
    onFetch() {
      this.boys = this.genderService.getNumberOf("B");
      this.girls = this.genderService.getNumberOf("G");
    }

    onToggleViewFalseVote(){
      this.hideStatusFalse = !this.hideStatusFalse;
      console.log("onToggleViewFalseVote => " + this.hideStatusFalse);
    }

    onPatch(id: number){
      this.spinnerTxt = 'Modification en cours...';
      this.spinner.show();

      this.genders= null;
      this.genderService.patchGender(id).subscribe(
        (response) => {
          this.genderService.loadAllFormsGender();
          this.spinner.hide();
        },
        (error) => {
         this.error = "Une erreur est survenue, veuillez re-essayer plus tard.";
         this.spinner.hide();
        }
      )
    }
  
    openDialog(genderId: number, genderEmail: string): void {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: "Voulez-vous vraiment supprimer le vote de '" + genderEmail + "' ?",
          });
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          this.onDelete(genderId);
        }
      });
    }
  
  
    onDelete(id: number){
      this.spinner.show();
      this.genderService.deleteGender(id)
        .pipe(first())
        .subscribe(
          (data) => {
            this.genderService.loadAllFormsGender();
            this.spinner.hide();
          },
          (error) => {
            if(error.status === 403){
              this.error = "Vous n'avez pas les droits pour supprimer ce vote."
            } else {
              this.error = "Une erreur est survenue, veuillez re-essayer plus tard."
            }
            this.spinner.hide();
          }
        );
      
    }

    scrollToElement(target): void {
      console.debug(target);
      document.getElementById(target).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }

}
