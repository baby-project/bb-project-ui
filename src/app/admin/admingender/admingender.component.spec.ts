import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmingenderComponent } from './admingender.component';

describe('AdmingenderComponent', () => {
  let component: AdmingenderComponent;
  let fixture: ComponentFixture<AdmingenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmingenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmingenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
