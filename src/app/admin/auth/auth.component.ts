import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@bbproject/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: string;
  error = '';

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private titleService: Title) { 

      if(this.authService.currentUserValue){
        this.router.navigate(['/parents']);
      }
    }

  ngOnInit() {
    this.initForm();

     // get return url from route parameters or default to '/'
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    /* Gestion du titre de la page */
    this.setTitle("Acces Parents Zone");
  }

  /**
    * Création du formulaire avec ses contraintes
    */
   initForm(){
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
  
   // convenience getter for easy access to form fields
   get f() { return this.loginForm.controls; }

  onSubmitForm(){
    this.spinner.show();

    if(this.loginForm.invalid){
      this.spinner.hide();
      return;
    }
    this.authService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (data) => {
            this.spinner.hide();
            window.location.replace([this.returnUrl].toString()); // need it to avoid cache
        },
        (error) => {
          if (error.status === 401) {
            this.error = "Mauvais nom d'utilisateur ou mot de passe";
          } else {
            this.error = "Il y a un soucis avec le serveur, veuillez re essayer plus tard";
          }
          this.spinner.hide();
        }
      )


  }
}
